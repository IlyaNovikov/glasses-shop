const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const reviews = new Schema({
    userName: String,
    comment: String,
    photo: String
})
module.exports = mongoose.model('reviews', reviews)