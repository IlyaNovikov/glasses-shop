const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const productSchema = new Schema(
    {
        title: String,
        description: String,
        image: String,
        mirror_frame: String,
        width: Number,
        category: String,
        material: String,
        brand: String,
        gender: String,
        versionKey: false
    },
)

module.exports = mongoose.model("products", productSchema)

