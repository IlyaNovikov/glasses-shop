const express = require('express');
const router = express.Router();
const controller = require('../controllers/reviewController');

router.get('/reviews', controller.getAll);
module.exports = router;