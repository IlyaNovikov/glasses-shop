const app = require('./app');
const port = process.env.PORT || 5000;
const express = require('express');
const path = require('path')
/*app.get('/', (req, res) => {
    res.status(200).json({
        message: 'work',
    })
})*/

app.use(express.static(__dirname + '/frontend/dist/course-project'));
app.listen(port, () => console.log(`Server is work on ${port}`));
