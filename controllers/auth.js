const bcrypt = require('bcryptjs');
const keys = require('../config/keys')
const jwt = require('jsonwebtoken');
const Admin = require('../models/Admin');
const errorHandler = require('../utils/errorHandler');

module.exports.login = async function (req, res) {
    const user = await Admin.findOne({email: req.body.email});

    if (user) {
        const passwordResult = bcrypt.compareSync(req.body.password, user.password);
        if (passwordResult) {
            const token = jwt.sign({
                email: user.email, adminId: user._id,
            }, keys.jwt, {expiresIn: 60 * 60});

            res.status(200).json({
                token: `Bearer ${token}`,
            })
        } else {
            res.status(404).json({
                message: "Пароль не верный!"
            })
        }

    } else {
        res.status(404).json({
            message: "Аккаунт не найден",
        })
    }
}


module.exports.register = async function (req, res) {
    const user = await Admin.findOne({email: req.body.email});

    if (user) {
        res.status(409).json({
            message: "Аккаунт существует",
        })
    } else {
        const salt = bcrypt.genSaltSync(10);
        const password = req.body.password;
        const admin = new Admin({
            email: req.body.email, password: bcrypt.hashSync(password, salt)
        })

        try {
            await admin.save();
            res.status(200).json(user)
        } catch (e) {
            errorHandler(res, e);
        }
    }
}
