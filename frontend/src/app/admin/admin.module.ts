import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {LoginPageComponent} from "./login-page/login-page.component";
import {AdminLayoutComponent} from "./shared/components/admin-layout/admin-layout.component";
import {AuthService} from "./shared/services/auth.service";
import {SharedModule} from "./shared/shared.module";
import {RegisterPageComponent} from './register-page/register-page.component';

@NgModule({
  declarations: [
    LoginPageComponent,
    AdminLayoutComponent,
    RegisterPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '', component: AdminLayoutComponent, children: [
          {path: '', redirectTo: '/admin/login', pathMatch: 'full'},
          {path: 'login', component: LoginPageComponent},
          {path: 'register', component: RegisterPageComponent}
        ]
      }
    ])
  ],
  exports: [RouterModule],
  providers: [AuthService]
})
export class AdminModule {
}
