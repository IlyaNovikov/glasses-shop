import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {User} from "../../../shared/interfaces";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";

@Injectable()
export class AuthService{
  constructor(private http: HttpClient) {

  }

  private token: any = null;

  login(user: User): Observable<any>{
    return this.http.post('/api/admin/auth/login', user)
      .pipe(
        tap((token: any) => {
          localStorage.setItem('auth-token', token);
          this.setToken(token);
        })
      );
  }

  logout() {
    this.setToken(null);
    localStorage.clear();

  }

  register(user: User): Observable<User> {
    return this.http.post<User>('api/admin/auth/register', user);
  }

  isAuthenticated (): boolean {
    return !!this.token;
  }
  setToken(token: any) {
    this.token = token;
  }
  getToken(): string {
    return this.token;
  }
}
