import {NgModule} from "@angular/core";
import {FilterPipe} from "./pipes/filter.pipe";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DragDropModule} from "@angular/cdk/drag-drop";

@NgModule({
  declarations: [FilterPipe],
  imports: [FormsModule,ReactiveFormsModule,DragDropModule],
  exports: [FilterPipe,FormsModule,ReactiveFormsModule, DragDropModule],
})
export class GuestSharedModule {

}
