import {Component, OnDestroy, OnInit} from '@angular/core';
import {FILTER_TYPES} from "../../../utils/constants";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {ApiService} from "../../../guest/services/api.service";
import {NavigationPath} from "../../navigation.interface";

@Component({
  selector: 'app-hide-menu',
  templateUrl: './hide-menu.component.html',
  styleUrls: ['./hide-menu.component.less']
})
export class HideMenuComponent implements OnInit, OnDestroy {
  NavigationPath = NavigationPath;
  categoryIsOpen = true;
  brandIsOpen = false;
  filterTypes = FILTER_TYPES;
  public categoryList: any = [];
  loading = false;
  public subscription: Subscription | undefined;

  constructor(private router: Router,
              private apiService: ApiService) {
  }

  ngOnInit(): void {
    this.categoryList = Object.values(this.filterTypes);
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

  openCategory() {
    this.categoryIsOpen = true;
    this.brandIsOpen = false;
  }

  openBrand() {
    this.categoryIsOpen = false;
    this.brandIsOpen = true;
  }

  navigateToCatalog(filter: string) {
    this.apiService.activeCategory = filter;
    if (filter) {
      this.router.navigate([NavigationPath.Catalog], {queryParams: {filter: filter}/*, queryParamsHandling: 'merge' */});
    } else {
      this.router.navigate([NavigationPath.Catalog]);
    }
  }
}
