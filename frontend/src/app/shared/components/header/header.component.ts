import {Component, OnInit} from '@angular/core';
import {CartService} from "../../../guest/services/cart.service";
import {LikedService} from "../../../guest/services/liked.service";
import {NavigationPath} from "../../navigation.interface";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
  NavigationPath = NavigationPath;
  toggle = false;
  public countItem: number = 0;
  public totalPrice: number = 0;
  public searchString = "";
  public favoriteItemsCount: number = 0;

  constructor(private cartService: CartService,
              private likedService: LikedService) {
  }

  ngOnInit(): void {
    this.cartService.getProducts()
      .subscribe((res: any) => {
        this.countItem = res.length;
        this.totalPrice = this.cartService.getTotalPrice();
      })
    this.likedService.getProducts()
      .subscribe((res: any) => {
        this.favoriteItemsCount = res.length;
      })
  }

  changeToggle() {
    this.toggle = !this.toggle;

  }

  search(event: any) {
    this.searchString = event.target.value;
    this.cartService.searchItem.next(this.searchString);
  }
}
