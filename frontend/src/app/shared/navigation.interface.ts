export enum NavigationPath {
  Home = '',
  Catalog = 'catalog',
  Product = 'catalog/:id',
  Cart = 'cart',
  Ordering = 'cart/order',
  Liked = 'liked-products',
  AboutCompany = 'about-company',
  Contacts = 'contacts',
  Fitting = 'fitting'
}

export interface NavigationLink {
  route: NavigationPath;
  label: string;
  params?: Record<string, any>;
}
