import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CartService {

  public cartItemList: any = [];
  public productList: any = new BehaviorSubject<any>([]);
  public searchItem = new BehaviorSubject<string>("");
  public placeholder: any;
  public products = [];

  constructor() {
    const ls = JSON.parse(localStorage.getItem('cart')!) || [];
    if (ls) {
      this.productList.next(ls);
      this.cartItemList = ls;
    } else {
      localStorage.setItem('cart', JSON.stringify(this.cartItemList));
    }
  }

  getProducts() {
    return this.productList.asObservable();
  }

  getCartItem(product: any) {
    const ls = JSON.parse(localStorage.getItem('cart')!) || [];
    let list = ls;
    return list.find((item: any) => {
      if (item._id === product._id) {
        return item;
      }
    })
  }

  // setProduct(product: any) {
  //   this.cartItemList.push(...product);
  //   this.productList.next(product);
  // }
  addToCart(product: any) {
    const ls = JSON.parse(localStorage.getItem('cart')!) || [];
    this.cartItemList = ls;
    let alreadyExistInCart = false;
    let existingCartItem = undefined;
    if (this.cartItemList.length > 0) {
      existingCartItem = this.cartItemList.find((el: any) => el._id === product._id);
      alreadyExistInCart = (existingCartItem != undefined);
    }
    if (alreadyExistInCart) {
      return;
    } else {
      if (ls) {
        const newData = [...ls, product];
        localStorage.setItem('cart', JSON.stringify(newData));
        this.cartItemList.push(product);
        this.productList.next(this.cartItemList);
      } else {
        this.cartItemList.push(product);
        localStorage.setItem('cart', JSON.stringify(this.cartItemList));
        this.productList.next(this.cartItemList);
      }
    }
    this.getTotalPrice();
  }

  getTotalPrice(): number {
    const totalPriceValue = this.cartItemList.reduce((totalPrice: number, product: any) => {
      return totalPrice += product.quantity * product.total;
    }, 0)
    return Math.round(totalPriceValue * 100) / 100;
  }

  removeCartItem(product: any) {
    const ls = JSON.parse(localStorage.getItem('cart')!);
    this.cartItemList = ls;
    this.cartItemList.map((item: any, index: any) => {
      if (product._id === item._id) {
        this.cartItemList.splice(index, 1);
      }
    })
    localStorage.setItem('cart', JSON.stringify(this.cartItemList));
    this.productList.next(this.cartItemList);
  }

  removeAllCart() {
    this.cartItemList = [];
    localStorage.setItem('cart', JSON.stringify(this.cartItemList));
    this.productList.next(this.cartItemList);
  }

  incrementQuantity(product: any) {
    this.cartItemList.map((item: any) => {
      if (item._id === product._id) {
        item.quantity++;
        product.quantity = item.quantity;
      }
    })
    localStorage.setItem('cart', JSON.stringify(this.cartItemList));
    this.productList.next(this.cartItemList);
  }

  decrementQuantity(product: any) {
    this.cartItemList.map((item: any) => {
      if (item._id === product._id) {
        item.quantity--;
        product.quantity = item.quantity;
      }
    })
    localStorage.setItem('cart', JSON.stringify(this.cartItemList));
    this.productList.next(this.cartItemList);
    if (!product.quantity) {
      this.removeCartItem(product);
    }
  }
}
