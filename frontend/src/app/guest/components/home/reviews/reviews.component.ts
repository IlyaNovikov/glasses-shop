import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ApiService} from "../../../services/api.service";
import {Subscription} from "rxjs";


@Component({
  selector: 'app-reviews', templateUrl: './reviews.component.html', styleUrls: ['./reviews.component.less']
})
export class ReviewsComponent implements OnInit, AfterViewInit {
  @ViewChild('firstSlide') public firstSlide: ElementRef | undefined;
  @ViewChild('secondSlide') public secondSlide: ElementRef | undefined;
  @ViewChild('thirdSlide') public thirdSlide: ElementRef | undefined;
  @ViewChild('fourthSlide') public fourthSlide: ElementRef | undefined;
  @ViewChild('fifthSlide') public fifthSlide: ElementRef | undefined;
  @ViewChild('init') public init: ElementRef | undefined;
  public subscription: Subscription | undefined;
  public reviews: any;

  public slideCounter: number = 0;

  constructor(private cdr: ChangeDetectorRef, private apiService: ApiService) {

  }

  ngAfterViewInit(): void {
    this.init?.nativeElement.click();
  }

  ngOnInit(): void {
    this.subscription = this.apiService.getReviews()
      .subscribe(res => this.reviews = res);

  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  slideClick(count: number): void {
    this.slideCounter = count;
  }
}
