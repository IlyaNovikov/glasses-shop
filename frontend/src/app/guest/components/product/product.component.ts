import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {ApiService} from "../../services/api.service";
import {switchMap} from "rxjs/operators";
import {Subscription} from "rxjs";
import {CartService} from "../../services/cart.service";
import {NavigationPath} from "../../../shared/navigation.interface";

@Component({
  selector: 'app-product', templateUrl: './product.component.html', styleUrls: ['./product.component.less']
})
export class ProductComponent implements OnInit, OnDestroy {
  NavigationPath = NavigationPath;
  public disabledBtn = false;
  public btnText = 'Добавить в корзину';
  public loading = false;
  public product: any;
  public imgUrl: string | undefined;
  public isAdded = false;
  public images = [];
  public productCount: string | undefined;
  public subscription: Subscription | undefined;
  public _widgetVisible: boolean = false;

  constructor(private route: ActivatedRoute, private apiService: ApiService, private cartService: CartService) {
  }

  ngOnInit(): void {
    this.loading = true;
    this.subscription = this.route.params
      .pipe(switchMap((params: Params) => {
        return this.apiService.getById(params.id)
      }))
      .subscribe((params: Params) => {
        this.product = {
          ...params, quantity: 1, total: params.price
        };

        this.checkCart(this.product)
        this.imgUrl = this.product.product.image;
        this.loading = false;
      })
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

  changeBigImage() {
  }

  addToCart() {
    this.btnText = 'В корзине';
    this.disabledBtn = true;
    this.cartService.addToCart(this.product);
  }

  incrementQuantity(product: any) {
    this.cartService.incrementQuantity(product);
  }

  decrementQuantity(product: any) {
    if (product.quantity > 1) {
      this.cartService.decrementQuantity(product);
    } else if (product.quantity <= 1) {
      this.btnText = 'Добавить в козину';
      this.disabledBtn = false;
      this.cartService.removeCartItem(product);
    }
  }

  checkCart(product: any) {
    let item = this.cartService.getCartItem(product);
    if (item) {
      this.product = item;
      this.btnText = 'В корзине';
      this.disabledBtn = true;
    } else {
      return;
    }
  }

  public _showWidget() {
    this._widgetVisible = true;
  }

  public _closeWidget() {
    this._widgetVisible = false;
  }
}
