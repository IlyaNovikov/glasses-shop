import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-widget', templateUrl: './widget.component.html', styleUrls: ['./widget.component.less']
})
export class WidgetComponent implements OnInit {
  @Input() public title: string = '';
  @Input() public desc: string = '';
  @Input() public glassesImg: string = '';
  @Input() public frameWidth: number = 135;
  public _isValid: boolean = false;
  public _uploadImg: string | ArrayBuffer | null = '';
  public _PD: number = 62;
  public _glassesStyle: { width: string; left: string; top: string, transform: string } = {
    width: '0px', left: '0', top: '0', transform: ''
  }
  public _userImgStyle: { transform: string, width: string } = {
    transform: '', width: ''
  }
  @Output() isConfirmed: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
  }

  public _closeWidget(): void {
    this.isConfirmed.emit(false);
  }

  ngOnInit(): void {
  }

  public uploadFile(e: any): void {
    const selectedFile: File = e.target.files[0];
    if (selectedFile.type.indexOf('jpeg') == -1 && selectedFile.type.indexOf('image') == -1 && selectedFile.type.indexOf('png') == -1) return;
    if (selectedFile) {
      const reader = new FileReader();
      reader.onload = () => {
        this._uploadImg = reader.result;
      };
      reader.readAsDataURL(selectedFile);
    }
  }

  public setPD(e: any): void {
    this._PD = e.target.value;
  }

  public setGlasses(): void {
    const leftDraggable = document.getElementById('leftDraggable') as HTMLElement;
    const rightDraggable = document.getElementById('rightDraggable') as HTMLElement;
    const crossLeft = getTransform(leftDraggable);
    const crossRight = getTransform(rightDraggable);
    const distanceBetweenPupilMarks = widget_getDistance(crossLeft, crossRight);
    let width = ((this.frameWidth / 100) / (this._PD / distanceBetweenPupilMarks)) * 100;
    this._glassesStyle.width = width + 'px';
    this._glassesStyle.left = crossLeft[0] - (width - distanceBetweenPupilMarks) / 2 + 'px';
    this._glassesStyle.top = crossLeft[1] - (width - distanceBetweenPupilMarks) / 4 + 'px';
    this._glassesStyle.transform = `rotate(${getAngle(crossLeft, crossRight)}deg)`;
    leftDraggable.style.visibility = 'hidden';
    rightDraggable.style.visibility = 'hidden';

    function getTransform(item: HTMLElement): number[] {
      const transArr: number[] = [];
      if (!window.getComputedStyle) {
        return transArr;
      }
      const style = window.getComputedStyle(item);
      const transform = style.transform || style.webkitTransform;
      let mat = transform.match(/^matrix\((.+)\)$/);
      mat ? transArr.push(parseInt(mat[1].split(', ')[4], 10) + item.offsetWidth / 2) : transArr.push(0);
      mat ? transArr.push(parseInt(mat[1].split(', ')[5], 10) + item.offsetHeight / 2) : transArr.push(0);
      return transArr;
    }

    function getAngle(crossLeft: number[], crossRight: number[]): number {
      let angle = Math.atan2(crossLeft[1] - crossRight[1], crossLeft[0] - crossRight[0]) * (180 / Math.PI) - 180;
      if (angle < 0) {
        angle += 360;
      }
      return angle;
    }

    function widget_getDistance(crossLeft: number[], crossRight: number[]): number {
      let diffX = Math.abs(crossLeft[0] - crossRight[0]);
      let diffY = Math.abs(crossLeft[1] - crossRight[1]);
      return Math.sqrt(diffX * diffX + diffY * diffY);
    }
  }

  public rotationImg(e: any): void {
    this._userImgStyle.transform = `rotate(${e.target.value}deg)`;
  }

  public resizeImg(e: any): void {
    this._userImgStyle.width = `${e.target.value}%`;
  }
}
