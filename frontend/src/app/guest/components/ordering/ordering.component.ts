import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CartService} from "../../services/cart.service";

@Component({
  selector: 'app-ordering', templateUrl: './ordering.component.html', styleUrls: ['./ordering.component.less']
})
export class OrderingComponent implements OnInit {
  userData: FormGroup | any;
  public cartItems: any | [];
  public totalPrice = 0;
  public _isSave: boolean=false

  constructor(private cartService: CartService) {
  }

  ngOnInit(): void {
    this.userData = new FormGroup({
      fio: new FormControl(null, [Validators.required, Validators.maxLength(50), Validators.pattern("^[а-яА-ЯёЁa-zA-Z ]+$")]),
      phone: new FormControl(null, [Validators.required, Validators.pattern("^\\+?[78][-\\(]?\\d{3}\\)?-?\\d{3}-?\\d{2}-?\\d{2}$")]),
      email: new FormControl(null, [Validators.required, Validators.email,]),
      address: new FormControl(null, [Validators.required, Validators.pattern("^[а-яА-ЯёЁa-zA-Z0-9 .()]+$")])
    });
    this.cartItems = JSON.parse(localStorage.getItem('cart')!) || [];
    this.totalPrice = this.cartService.getTotalPrice();
  }

  submit() {
    this._isSave=true;
    this.cartService.removeAllCart()
  }
}
