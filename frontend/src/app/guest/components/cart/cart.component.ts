import {Component, OnDestroy, OnInit} from '@angular/core';
import {CartService} from "../../services/cart.service";
import {Subscription} from "rxjs";
import {NavigationPath} from "../../../shared/navigation.interface";

@Component({
  selector: 'app-cart', templateUrl: './cart.component.html', styleUrls: ['./cart.component.less']
})
export class CartComponent implements OnInit, OnDestroy {
  NavigationPath = NavigationPath;
  public products: any = [];
  public grandTotal !: number;
  public subscription: Subscription | undefined;


  constructor(private cartService: CartService) {
  }


  ngOnInit(): void {
    this.updateProducts()
  }

  updateProducts() {
    this.subscription = this.cartService.getProducts()
      .subscribe((res: any) => {
        this.products = res;
        this.grandTotal = this.cartService.getTotalPrice();
      })
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

  removeProduct(product: any) {
    this.cartService.removeCartItem(product);
  }

  incrementQuantity(product: any) {
    this.cartService.incrementQuantity(product);
  }

  decrementQuantity(product: any) {
    this.cartService.decrementQuantity(product);
    this.updateProducts();
  }

  emptyCart() {
    this.cartService.removeAllCart();
  }
}
