import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainLayoutComponent} from "./shared/components/main-layout/main-layout.component";
import {NavigationPath} from "./shared/navigation.interface";


const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {path: '', loadChildren: () => import('./guest/home-page/home-page.module').then(mod => mod.HomePageModule),},
      {
        path: NavigationPath.Catalog,
        loadChildren: () => import('./guest/components/catalog.module').then(mod => mod.CatalogModule)
      },
      {
        path: NavigationPath.Liked,
        loadChildren: () => import('./guest/components/liked.module').then(mod => mod.LikedModule)
      },
      {
        path: NavigationPath.Cart,
        loadChildren: () => import('./guest/components/cart/cart.module').then(mod => mod.CartModule)
      },
    ]
  },
  {
    path: 'admin', loadChildren: () => import('./admin/admin.module').then(mod => mod.AdminModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
